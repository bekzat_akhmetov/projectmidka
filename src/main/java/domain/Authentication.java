package domain;

public class Authentication {
    private int token;

    public Authentication(int token) {
        this.token = token;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }
}
