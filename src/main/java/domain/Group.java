package domain;

public class Group {
    private long id;
    private long group_id;
    private long user_id;

    public Group(long id, long group_id, long user_id) {
        this.id = id;
        this.group_id = group_id;
        this.user_id = user_id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getGroup_id() {
        return group_id;
    }

    public void setGroup_id(long group_id) {
        this.group_id = group_id;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", group_id=" + group_id +
                ", user_id=" + user_id +
                '}';
    }
}
