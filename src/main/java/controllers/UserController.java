package controllers;

import domain.User;
import org.glassfish.jersey.media.multipart.FormDataParam;
import services.UserInteractor;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

@Path("users")
public class UserController {
    private UserInteractor userInteractor;

    public UserController() {
        userInteractor = new UserInteractor();
    }
    @GET
    public String hello(){
        return "Bekzat";
    }

    @GET
    @Path("/{id}")
    public Response getUserByID(@PathParam("id") long id) {
        User user = userInteractor.getUserByID(id);
        if (user == null) {
            return Response.
                    status(Response.Status.NOT_FOUND)
                    .entity("There is no user with such id!")
                    .build();
        } else {
            return Response.
                    status(Response.Status.OK).
                    entity(user).
                    build();
        }
    }

    @GET
    @Path("/{id}/remove")
    public Response deleteUserByID(@PathParam("id") long id) {
        boolean beka=userInteractor.deleteUserByID(id);
       if(beka==true){
           return Response
                .status(Response.Status.OK)
                .entity("Successfully deleted")
                .build();
       }
       else{
           return Response
                   .status(Response.Status.NOT_FOUND)
                   .entity("There is no user with such id!")
                   .build();
       }
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Path("/add")
    public Response addUser(@FormDataParam("name") String name,
                            @FormDataParam("surname") String surname,
                            @FormDataParam("username") String username,
                            @FormDataParam("password") String password,
                            @FormDataParam("birthday") String birthday) throws SQLException {
        userInteractor.addUser(name, surname, username, password, birthday);
        return Response.status(200).entity("User created").build();
    }

    @POST
    @Path("/delete")
    public Response deleteUser(@FormDataParam("id") long id) throws SQLException {
        userInteractor.deleteUser(id);
        return Response.status(200).entity("Successfully deleted").build();
    }

}
