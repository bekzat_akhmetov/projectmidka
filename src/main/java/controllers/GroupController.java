package controllers;

import domain.User;
import services.GroupInteractor;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("groups")
public class GroupController {
    private GroupInteractor groupInteractor;

    public GroupController(){
        groupInteractor=new GroupInteractor();
    }

    @GET
    public String index(){
    return "Group controller!";
    }

    @GET
    @Path("/{param}/users")
    public Response getGroupUsersByGroupID(@PathParam("param") long id){
        Iterable<User>users=groupInteractor.getUsersByGroupID(id);
        return Response.
                status(Response.Status.OK)
                .entity(users)
                .build();
    }
}
