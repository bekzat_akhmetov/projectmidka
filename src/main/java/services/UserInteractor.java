package services;

import domain.User;
import repositories.entities.UserRepository;
import repositories.inerfaces.IEntityRepository;

import java.sql.Date;
import java.sql.SQLException;
import java.util.LinkedList;

public class UserInteractor {
    private IEntityRepository userRepo;

    public UserInteractor() {
        userRepo = new UserRepository();
    }

    public User getUserByID(long id) {
        LinkedList<User> users = (LinkedList<User>) userRepo.query("SELECT * FROM myuser WHERE id= " + id);
        return (users.isEmpty() ? null : users.get(0));
    }
    public boolean deleteUserByID(long id){
        userRepo.query("DELETE FROM myuser WHERE id=" +id);
        try{
        return true;
        } catch (Exception ex){
            return false;
        }
    }

    public void addUser(String name, String surname, String username, String password, String birthday) throws SQLException {
        User user=new User(name,surname,username,password,Date.valueOf(birthday));
        userRepo.add(user);
    }
    public void deleteUser(long id) throws SQLException {
        User user=new User(id);
        userRepo.remove(user);
    }
}
