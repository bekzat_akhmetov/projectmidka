package services;

import domain.User;
import repositories.entities.UserRepository;
import repositories.inerfaces.IEntityRepository;

public class GroupInteractor {
    private IEntityRepository userRepo;

    public GroupInteractor() {
        userRepo = new UserRepository();
    }

    public Iterable<User> getUsersByGroupID(long id) {
        String sql = "select u.*from group_user_mapping m,groups g,myuser u where m.user_id=u.id and m.group_id=g.id and g.id= " + id;
        Iterable<User> users = userRepo.query(sql);
        return users;
    }
}
