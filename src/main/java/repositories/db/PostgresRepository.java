package repositories.db;

import repositories.inerfaces.IDBRepository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class PostgresRepository implements IDBRepository {

    @Override
    public Connection getConnection() {
        try {
            String connStr="jdbc:postgresql://localhost:5432/users";
            Connection conn=DriverManager.getConnection(connStr,"postgres","040916");
            return conn;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return null;
    }
}