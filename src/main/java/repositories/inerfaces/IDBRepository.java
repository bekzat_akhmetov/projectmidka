package repositories.inerfaces;

import java.sql.Connection;

public interface IDBRepository {
    Connection getConnection();
}
