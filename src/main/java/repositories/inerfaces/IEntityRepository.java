package repositories.inerfaces;

import java.sql.SQLException;

public interface IEntityRepository<T> {
    void add(T entity) throws SQLException;
    void update(T entity);
    void remove(T entity) throws SQLException;
    Iterable<T> query(String sql);
}
