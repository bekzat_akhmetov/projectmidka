package repositories.entities;

import domain.Group;
import repositories.db.PostgresRepository;
import repositories.inerfaces.IDBRepository;
import repositories.inerfaces.IEntityRepository;

import java.sql.SQLException;
import java.sql.Statement;

public class GroupRepository implements IEntityRepository<Group> {
    private IDBRepository dbrepo;

    public GroupRepository() {
        dbrepo = new PostgresRepository();
    }

    @Override
    public void add(Group entity)  {
        try {
            Statement stmt = dbrepo.getConnection().createStatement();
            String sql = "INSERT INTO group_user_mapping (id,group_id,user_id) values ('" + entity.getGroup_id() + "','" + entity.getUser_id() + "')";
            stmt.execute(sql);
        }
        catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
    }

    @Override
    public void update(Group entity) {

    }

    @Override
    public void remove(Group entity) throws SQLException {

    }

    @Override
    public Iterable<Group> query(String sql) {
        return null;
    }
}
